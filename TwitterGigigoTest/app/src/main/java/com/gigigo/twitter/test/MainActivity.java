package com.gigigo.twitter.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;


public class MainActivity extends AppCompatActivity {


    private TwitterLoginButton loginButton;
    private String DEBUG_TAG="TWITTER_TEST";

    private Button btnLogin,btnGetEmail,btnTestRestApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginButton = (TwitterLoginButton) findViewById(R.id.login_button);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnGetEmail = (Button) findViewById(R.id.btnGetEmail);
        btnTestRestApi = (Button) findViewById(R.id.btnTestRestApi);

        btnLogin.setVisibility(View.GONE);
        btnGetEmail.setVisibility(View.GONE);
        btnTestRestApi.setVisibility(View.GONE);

        //Incializamos el botón de Twitter
        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {

                TwitterSession sessionResult = result.data;
                getSessionData(sessionResult);

                btnLogin.setVisibility(View.VISIBLE);
                btnGetEmail.setVisibility(View.VISIBLE);
                btnTestRestApi.setVisibility(View.VISIBLE);
                loginButton.setVisibility(View.GONE);

            }

            @Override
            public void failure(TwitterException exception) {
                Log.i(DEBUG_TAG, "fail");
            }
        });


        btnGetEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMail();
            }
        });

        btnTestRestApi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //REST API

                TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
                // Can also use Twitter directly: Twitter.getApiClient()
                StatusesService statusesService = twitterApiClient.getStatusesService();
                statusesService.show(524971209851543553L, null, null, null, new Callback<Tweet>() {
                    @Override
                    public void success(Result<Tweet> result) {
                        //Do something with result, which provides a Tweet inside of result.data
                        Log.i(DEBUG_TAG, result.data.text);
                    }

                    public void failure(TwitterException exception) {
                        //Do something on failure
                    }
                });

            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, ShowTweetsActivity.class);
                startActivity(myIntent);
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the login button.
        loginButton.onActivityResult(requestCode, resultCode, data);
    }



    private void getSessionData(TwitterSession sessionSessionManager){

        TwitterAuthToken authToken = sessionSessionManager.getAuthToken();
        String token = authToken.token;
        String secret = authToken.secret;

        Log.i(DEBUG_TAG, "token: " + token + " secret: " + secret);

    }

    private void getMail(){

        TwitterSession sessionSessionManager = Twitter.getSessionManager().getActiveSession();

        TwitterAuthClient authClient = new TwitterAuthClient();
        authClient.requestEmail(sessionSessionManager, new Callback<String>() {
            @Override
            public void success(Result<String> result) {
                // Do something with the result, which provides the email address
                Toast.makeText(MainActivity.this, "email: " + result.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(TwitterException exception) {
                Log.i(DEBUG_TAG, "fail");
            }
        });

    }
}
