package com.gigigo.twitter.test;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;
import com.twitter.sdk.android.tweetui.CompactTweetView;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.TweetUtils;
import com.twitter.sdk.android.tweetui.TweetView;
import com.twitter.sdk.android.tweetui.UserTimeline;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * Created by tico on 04/08/15.
 */
public class ShowTweetsActivity extends AppCompatActivity {

    private final long tweetId = 596648144538075136L;
    private final List<Long> tweetIds = Arrays.asList(627876095384158209L, 628663208576962560L,628684446166843392L,609512628617125888L);
    private ListView listTimeline;
    private ScrollView showTweetsTestContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_tweets);

        final  TweetView tweetView = (TweetView) findViewById(R.id.tweetView);
        final  LinearLayout myLayout = (LinearLayout) findViewById(R.id.tweet_container);
        final  Button btnLoadTweet = (Button)findViewById(R.id.btnLoadTweet);
        final  Button btnLoadTweets = (Button)findViewById(R.id.btnLoadTweets);
        final  Button btnLoadTimeline = (Button)findViewById(R.id.btnLoadTimeline);
        listTimeline =(ListView)findViewById(R.id.listTimeline);
        showTweetsTestContainer=(ScrollView) findViewById(R.id.showTweetsTestContainer);

        btnLoadTweet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TweetUtils.loadTweet(tweetId, new Callback<Tweet>() {
                    @Override
                    public void success(Result<Tweet> result) {
                        myLayout.removeAllViews();
                        myLayout.addView(new TweetView(ShowTweetsActivity.this, result.data));
                        toggleListVisibilty(false);

                    }

                    @Override
                    public void failure(TwitterException exception) {
                        // Toast.makeText(...).show();
                    }
                });

            }
        });
        btnLoadTweets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TweetUtils.loadTweets(tweetIds, new Callback<List<Tweet>>() {
                    @Override
                    public void success(Result<List<Tweet>> result) {
                        myLayout.removeAllViews();
                        for (Tweet tweet : result.data) {
                            myLayout.addView(new CompactTweetView(ShowTweetsActivity.this, tweet, R.style.tw__TweetLightStyle));
                        }
                        toggleListVisibilty(false);

                    }

                    @Override
                    public void failure(TwitterException exception) {
                        // Toast.makeText(...).show();
                    }
                });
            }
        });

        btnLoadTimeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadTimeline();
            }
        });
    }

    private void loadTimeline(){
        UserTimeline userTimeline = new UserTimeline.Builder().screenName("BBVABancomer").build();
        final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter(this, userTimeline);
        listTimeline.setAdapter(adapter);

        toggleListVisibilty(true);
    }

    private void toggleListVisibilty(boolean showList){
        if(showList){
            listTimeline.setVisibility(View.VISIBLE);
            showTweetsTestContainer.setVisibility(View.GONE);

        }else {

            listTimeline.setVisibility(View.GONE);
            showTweetsTestContainer.setVisibility(View.VISIBLE);


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_compose_tweet:
                composeTweet();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void composeTweet() {

        File myImageFile = new File(Environment.getExternalStorageDirectory().getPath() +"/gigigo.png");
        Uri myImageUri = Uri.fromFile(myImageFile);

        TweetComposer.Builder builder = new TweetComposer.Builder(this)
                .text("¡Tweet de prueba!")
                .image(myImageUri);
        builder.show();

    }

}
