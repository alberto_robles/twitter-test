package com.gigigo.twitter.test;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

/**
 * Created by tico on 31/07/15.
 */
public class TwitterGigigoTest extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "MlrvhMi6ZWMg7EThpPu0DQIQS";
    private static final String TWITTER_SECRET = "7zBahA4wASAQvyxjeSDINGpv7Zup4h6WJqEABsbsveuSZ6Hqzt";

    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig), new Crashlytics());

    }
}
